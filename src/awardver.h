/**********************************************
**
**
**	Defines for AwardVer
**
**
***********************************************/

#include	<stdio.h>

#ifdef __LINUX_NOW__
    #define SftPlatform         "Linux"
#else
    #define SftPlatform         "DOS"
#endif

#define SftVersion      "0.2 ("SftPlatform")"
