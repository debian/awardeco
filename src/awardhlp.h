/**********************************************                                 
**                                                                              
**                                                                              
**      Defines for AwardHelp                                                    
**                                                                              
**                                                                              
***********************************************/                                
                                                                                
#include        <stdio.h>                                                       

byte HelpSystem(byte argc, byte *argv[])
{
	byte x;


		for( x = 1; x < argc; x++ )
		{
			if( StrCmp( argv[x], "-h" ) == 0 )
			{
				printf("\n"SftName" HelpSystem Starting Now!\n");
				printf("\nThis Program Version Number %s",SftVersion);
				

#ifndef __LINUX_NOW__			
				printf(
				"\n"SftName" - Decompressor for AwardBIOSes only.\n"
				"\tSupported formats: AwardBIOS 4.51, 6.0 and later\n\n"
				""SftName" performs on 386 or better CPU systems\n"
				"under control of DOS, Win3.xx, Win9x/NT/2K or DosEmu\n\n"
				"\tBug reports mailto: "SftEMail"\n"
				"\t\tCompiled: %s, %s\n",__DATE__,__TIME__);
#else
				printf(
				"\n"SftName" - Decompressor for AwardBIOSes only.\n"
				"\tSupported formats: AwardBIOS 4.51, 6.0 and later\n\n"
				""SftName" performs on 386 or better CPU systems\n"
				"under control of LinuxOS\n\n"
				"\tBug reports mailto: "SftEMail"\n"
				"\t\tCompiled: %s, %s with \n\t\t%s",__DATE__,__TIME__,__VERSION__);
#endif		

				printf("\n");
				return(Help);
			}

			if( StrCmp( argv[x], "-xr" )  == 0 )	return(XtractM);
			if( StrCmp( argv[x], "-xa" )  == 0 )	return(XtractA);
			if( StrCmp( argv[x], "-x" )   == 0 )	return(Xtract);
			if( StrCmp( argv[x], "-la" )  == 0 )	return(ListA);
			if( StrCmp( argv[x], "-l" )   == 0 )	return(List);
			if( StrCmp( argv[x], "-c" )   == 0 )	return(Copyrights);

		}
	return(0);
}

void PrintHeader(byte* EOL)
    {
    printf("\n%c%s%c%s", 0x4, SoftName, 0x4, EOL);
    }

void PrintUsage()
    {
	    PrintHeader("");
	    printf("%s", CopyRights);
	    printf("\n\nUsage: %s <BIO.BIN> [Options]",SftName);
	    printf(
	    "\n"
	    "\t\tOptions:"
	    "\n\t\t\t\"-l\" List Bios Structure"
	    "\n\t\t\t\"-x\" eXtract Bios Modules"
	    "\n\t\t\t\"-la\" List All Bios Structure"
	    "\n\t\t\t\"-xa\" eXtract All Bios Modules"
	    "\n\t\t\t\"-xr\" eXtract Bios Modules as Raw"
	    "\n\t\t\t\"-h\" Help statistics"
	    );

	    printf("\n\n\t*%s*\n",Url);
	    
    }
