/**********************************************                                 
**                                                                              
**                                                                              
**      Defines & Functions for AwarDeco
**                                                                              
**                                                                              
***********************************************/                                

#ifndef AWARDECO_H                                                                                                                             
#define AWARDECO_H 1

#include	<stdio.h>

typedef unsigned char	byte;
typedef unsigned short	word;
typedef unsigned long	dword;

#define	Xtract 		0x10
#define	List   		0x11
#define	XtractM 	0x20
#define	Copyrights 	0x30
#define XtractA         0x40
#define ListA           0x41
#define	Help	 	0x80

#define	BLOCK		0x8000

#ifndef __LINUX_NOW__                                                           
    #define	IDSign          "."
#else
    #define	IDSign          "+"
#endif


typedef struct
	{
		byte	HeadLen;
		byte	HeadCrc;
		byte	Method[5];
		dword	PackLen;
		dword	RealLen;
		dword	TStamp;
		byte	Attr;
		byte	Level;
		byte	FilenameLen;
		byte	FileName[12];
		word	CRC16;
		byte	DOS;
		word	Empty;
	} LZHHead;

typedef struct
	{
                byte    hdr_size;                                               
                byte    hdr_crc;                                                
                byte    method[5];                                              
                dword   pack_len;                                               
                dword   real_len;                                               
                dword   real_cs;                                                
                byte    rsrv1;                                                  
                byte    rsrv2;                                                  
                byte    name_len;                                               
        } LZHHeader;

typedef struct
        {
                byte    HeadLen;
                byte    HeadCrc;
                byte    Method[5];
                dword   PackLen;
                dword   RealLen;
                dword   TStamp;
                byte    Attr;
                byte    Level;
                byte    FilenameLen;
                byte    FileName[128];
        } NewLZHHead;

byte* GetModuleName(word ID);
	    
byte StrLen(byte *Str);
byte StrCmp(byte *Dst, byte *Src);

byte *GetFullDate(byte *mon, byte *day, byte *year);

byte Compute_Head_CRC(FILE *ptx, dword Offset,byte hdr_size);

/*---------------------------------
	FindHook
----------------------------------*/
dword FoundAt(FILE *ptx, byte *Buf, byte *Pattern, dword BLOCK_LEN);

/*---------------------------------
	XtractAwd
----------------------------------*/
byte XtractAwd(FILE *ptx, byte Action, dword DecoOff, dword FirstOff, byte isASUS);

/*---------------------------------
	TotalSecA
----------------------------------*/
byte TotalSecA(FILE *ptx, byte *Buf, byte Action, dword BootBlockOffset);

#endif
