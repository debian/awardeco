/**********************************************
**
**
**	Defines for AwardSoft
**
**
***********************************************/

#include	<stdio.h>
#include	"./awardver.h"

#define         SftName         "AwardBIOSDeco"                                   
#define         SftEMail        "anton.borisov@gmail.com"

        /********SoftWare*************/                                         
        byte SoftName[]         = "-="SftName", version "SftVersion"=-";
        byte CopyRights[]       = "\n(C) Anton Borisov, 2003, 2006. Portions (C) 1999-2000";
	byte Url[]              = "Bug-reports direct to "SftEMail;
									  
