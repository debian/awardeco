/*
 *      AwardBIOS Decompress                                                  
 *                                                                              
 *      Copyright (C) 2000, 2003 Anthony Borisow                                
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2, or (at your option)          
 * any later version.                                                           
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *                                                                              
 * You should have received a copy of the GNU General Public License            
 * along with this program; see the file COPYING.  If not, write to             
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.        
 */

#include        <stdio.h>                                                       
#include        <stdlib.h>                                                      

#if defined(LINUX) || defined(__LINUX__) || defined(__linux__)
    #include    <memory.h>
    #define     __LINUX_NOW__
#else
    #include    <mem.h>
    #include    <conio.h>
#endif

#include	"./awardeco.h"
#include	"./awardsft.h"
#include	"./awardver.h"
#include	"./awardhlp.h"

int main(byte argc, byte *argv[])
	{

		dword fLen, RealRead = 0;
		byte *BufBlk;

		LZHHead head;

		byte PartTotal = 0;
		byte Action = 0;

        FILE *ptx, *plain, *bios, *rom, *pto;
        byte CntMods = 0, tmp, crc, isASUS = 0;                                             
        dword Offset = 0;                                                       
        dword ROM_File_Size, i, DecoOff = 0, BootOff = 0, Packed = 0, Real = 0;
	dword FirstOff = 0xFFFFFFFF;
        dword DecoLen = 0x1000;
        word AllCRC = 0;
        byte AwardDeco[] = "= Award Decompre";
        byte AwardBoot[] = "Award BootBlock ";
	byte AwardHook[] = "-lh5-";
	byte ASUSHOOK[] = "ASUS_FLASH";
        byte* BufZone;
	

		switch( HelpSystem(argc,argv) ){
			case Help:	return 0;
			case Xtract:	Action = Xtract; break;
			case List:	Action = List; break;
			case XtractM:	Action = XtractM; break;
			case XtractA:   Action = XtractA; break;
			case ListA:     Action = ListA; break;
			case Copyrights:	Action = Copyrights; break;
			default:
				PrintUsage();
				printf("\n");
				return 0;
			}


		PrintHeader("\n\n");
		
		if( (ptx = fopen(argv[1],"rb")) == NULL )
			{
			printf("\nFATAL ERROR: File %s opening error...\n", argv[1]);
			return 0;
			};

		fseek(ptx, 0, 2); ROM_File_Size = ftell(ptx); rewind(ptx);
		fLen = ROM_File_Size;

		printf("\n\tAwardBIOS information:");
		printf("\nFileLength\t: %lX (%lu bytes)", ROM_File_Size, ROM_File_Size);
		printf("\nFileName\t: %s",argv[1]);

		/*------- Memory Alloc --------*/


		if( (BufBlk = (byte*)calloc(BLOCK, sizeof(byte)))==NULL ) exit(1);

		i = 0;
		while(!feof(ptx))
		{
			fseek(ptx, i, 0);
			/*--------- Where's AwardDeco? ------------*/
			RealRead = fread(BufBlk, 1, BLOCK, ptx);
			if((i = FoundAt(ptx, BufBlk, AwardDeco, RealRead)) != 0)
			{
			DecoOff = i;
			break;
			}
			i = ftell(ptx) - 0x100;

		}

		i = 0;
		while(!feof(ptx))
		{
			fseek(ptx,i,0);
			/*--------- Where's AwardBoot? ------------*/
			RealRead = fread(BufBlk, 1, BLOCK, ptx);
			if( (i = FoundAt(ptx, BufBlk, AwardBoot, RealRead)) != 0 )
			{
			BootOff = i;
			break;
			}
			i = ftell(ptx) - 0x100;

		}

		i = 0;
		while(!feof(ptx))
		{
			fseek(ptx,i,0);
			/*--------- Where's First Module? ------------*/
			RealRead = fread(BufBlk, 1, BLOCK, ptx);
			if( (i = FoundAt(ptx, BufBlk, AwardHook, RealRead)) != 0 )
			{
			FirstOff = (i - 2);
			break;
			}
			i = ftell(ptx) - 0x100;

		}

		i = 0;
		while(!feof(ptx))
		{
			fseek(ptx,i,0);
			/*--------- is it ASUS? ------------*/
			RealRead = fread(BufBlk, 1, BLOCK, ptx);
			if( (i = FoundAt(ptx, BufBlk, ASUSHOOK, RealRead)) != 0 )
			{
			isASUS = 1;
			break;
			}
			i = ftell(ptx) - 0x100;

		}

		printf("\nDecompression\t\t: %.4X", DecoOff);
		printf("\nBootBlock\t\t: %.4X", BootOff);
		printf("\nFirst Module\t\t: %.4X", FirstOff);
		printf("\nASUS?\t\t\t: %s", isASUS?("YES"):("NO"));
		    if(isASUS) printf("\nASUS Hook\t\t: %.4X", i);

		if ( (DecoOff == BootOff) )
		{
                   printf("\n\nSearched through file. Not an AwardBIOS\n");
                   exit(1);
                }


	switch(Action)
	{
	case Xtract:
	case XtractM:
	case List:
	case Copyrights:
		PartTotal = XtractAwd(ptx, Action, DecoOff, FirstOff, isASUS);
			break;
	case XtractA:
	case ListA:
		PartTotal = TotalSecA(ptx, BufBlk, Action, BootOff);
			break;
	default: break;
	}
	printf("\nTotal Sections\t: %i", PartTotal);

		free(BufBlk);
		
		printf("\n");
		return 0;
	}
